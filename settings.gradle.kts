rootProject.name = "slack-it-later"
include("core")
include("cli")
include("web")
include("rest")
