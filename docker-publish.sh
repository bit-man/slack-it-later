#!/usr/bin/env bash

./docker-build.sh && \
  docker login registry.gitlab.com && \
  docker push registry.gitlab.com/bit-man/slack-it-later:latest
