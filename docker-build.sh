#!/usr/bin/env bash

./build.sh && \
  mv rest/build/libs/rest-fat-*.jar rest/build/libs/rest-fat.jar && \
  docker build -t registry.gitlab.com/bit-man/slack-it-later:latest .
