FROM openjdk:11

COPY rest/build/libs/rest-fat.jar /opt
CMD java -jar /opt/rest-fat.jar
