package guru.bitman.slackitlater.server

class SettingsFolder(private val configuration: Configuration) {
    fun create() {
        configuration.logFolder.mkdirs()
    }
}