package guru.bitman.slackitlater.server

import java.io.File

data class Configuration(val port: String, val logFolder: File)
