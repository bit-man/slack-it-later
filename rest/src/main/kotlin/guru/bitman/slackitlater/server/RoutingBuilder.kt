package guru.bitman.slackitlater.server

import guru.bitman.slackitlater.actions.ListScheduledMessages
import guru.bitman.slackitlater.actions.ScheduleMessage
import guru.bitman.slackitlater.infrastructure.SlackMessageScheduler
import guru.bitman.slackitlater.infrastructure.SlackSchedulesLister
import guru.bitman.slackitlater.model.SchedulesDeletor
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import io.ktor.util.pipeline.*
import java.util.*


class RoutingBuilder(private val routing: Routing) {
    fun root(): RoutingBuilder {
        routing.get("/") {
            logRequest(null)
            call.respondText(contentType = ContentType.Text.Html, provider = indexHtmlProvider())
        }
        return this
    }

    private fun indexHtmlProvider(): suspend () -> String {
        return {
            WebResource(INDEX_HTML).readText()
        }
    }

    fun webJS(): RoutingBuilder {
        routing.get("/${WEB_JS}") {
            logRequest(null)
            call.respondText(contentType = ContentType.Text.JavaScript, provider = webJSProvider())
        }
        return this
    }

    private fun PipelineContext<Unit, ApplicationCall>.logRequest(payload: Any?) {
        val body = logEntry("Body", payload)
        val request = "Endpoint : ${call.request.httpMethod.value} - ${call.request.path()}"
        val headers = logMessage("Request Headers", call.request.headers)
        val queryParams = logMessage("Query parameters", call.request.queryParameters)
        call.application.environment.log.trace("${request}\n${headers}\n${queryParams}\n${body}\n--------------------")
    }

    private fun logMessage(title: String, parameters: StringValues): String {
        val entries = parameters.entries()
                .map { it.key to it.value.joinToString(",") }
                .joinToString("\n") { "${it.first}:${it.second}" }
        return logEntry(title, entries)
    }

    private fun logEntry(title: String, entries: Any?) = "\n[$title]\n${entries}"

    private fun webJSProvider(): suspend () -> String {
        return {
            WebResource(WEB_JS).readText()
        }
    }

    fun querySchedules(): RoutingBuilder {
        routing.get("/schedules") {
            val driver = SlackSchedulesLister(getToken())
            val scheduledMessages = ListScheduledMessages(driver)()
            logRequest(null)
            call.respond(scheduledMessages)
        }
        return this
    }

    fun newSchedule(): RoutingBuilder {
        routing.post(SINGLE_SCHEDULE) {
            val driver = SlackMessageScheduler(getToken())
            val newSchedule = call.receive<NewSchedule>()
            val postAt = Date(newSchedule.postAt)
            try {
                ScheduleMessage(driver)(newSchedule.channel, newSchedule.message, postAt)
                logRequest(newSchedule)
                call.respond(HttpStatusCode.Created, "Message scheduled")
            } catch (e: Exception) {
                logRequest(newSchedule)
                call.application.environment.log.error("New schedule failed", e)
                throw e
            }
        }
        return this
    }

    private fun PipelineContext<Unit, ApplicationCall>.getToken() =
            call.request.headers[TOKEN]

    fun build(): Routing {
        return routing
    }

    fun deleteSchedule(messageDeletor: SchedulesDeletor): RoutingBuilder {
        routing.delete(SINGLE_SCHEDULE) {
            val deleteSchedule = call.receive<DeleteSchedule>()
            try {
                messageDeletor.invoke(deleteSchedule.channel, deleteSchedule.messageId, getToken())
                logRequest(deleteSchedule)
                call.respond(HttpStatusCode.OK, "Message schedule deleted")
            } catch (e: Exception) {
                logRequest(deleteSchedule)
                call.application.environment.log.error("Message schedule deletion failed", e)
                call.respond(HttpStatusCode.InternalServerError)
            }
        }
        return this
    }

    private companion object {
        private const val WEB_JS = "web.js"
        private const val INDEX_HTML = "index.html"
        private const val SINGLE_SCHEDULE = "/schedule"
        private const val TOKEN = "Sil-Token"
    }

}

data class DeleteSchedule(val channel: String, val messageId: String)

data class NewSchedule(val channel: String, val message: String, val postAt: Long)
