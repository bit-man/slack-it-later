package guru.bitman.slackitlater.server

import guru.bitman.slackitlater.infrastructure.SlackScheduledMessageDeletor
import guru.bitman.slackitlater.model.SchedulesDeletor
import io.ktor.application.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import java.io.File

class Main {
    companion object {
        private lateinit var configuration: Configuration

        @JvmStatic
        fun main(vararg args: String) {
            init()
            server().start(wait = true)
        }

        private fun init() {
            configuration = Configuration(getPort(), LOGS_FOLDER)
            SettingsFolder(configuration).create()
        }

        private fun getPort() = System.getProperty(PORT_PROPERTY, PORT_DEFAULT)

        fun Application.serverModule(messageDeletor: SchedulesDeletor, configuration: Configuration) {
            ApplicationBuilder(this, configuration).withJackson().allowLocalhostResourceConsumption().build()
            routing {
                RoutingBuilder(this).root().webJS().querySchedules().newSchedule().deleteSchedule(messageDeletor).build()
            }
        }

        private fun server() = embeddedServer(Netty, port = configuration.port.toInt(), module = { serverModule(SlackScheduledMessageDeletor(), configuration) })
        private const val PORT_DEFAULT: String = "8080"
        private const val PORT_PROPERTY: String = "sil.port"
        private val HOME_FOLDER = File(System.getProperty("user.home"))
        private val SETTINGS_FOLDER = File(HOME_FOLDER, ".slack-it-later")
        private val LOGS_FOLDER = File(SETTINGS_FOLDER, "logs")
    }

}

