package guru.bitman.slackitlater.server

class WebResource(private val filename: String) {
    fun readText() = getFile(filename).readText(Charsets.UTF_8)
    private fun getFile(filename: String) = Main::class.java.getResource("/$filename")
}