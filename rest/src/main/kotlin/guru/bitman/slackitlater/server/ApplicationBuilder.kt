package guru.bitman.slackitlater.server

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.jackson.*

class ApplicationBuilder(private val app: Application, private val configuration: Configuration) {
    fun withJackson(): ApplicationBuilder {
        app.install(ContentNegotiation) {
            jackson {
            }
        }
        return this
    }

    fun build(): Application {
        return app
    }

    fun allowLocalhostResourceConsumption(): ApplicationBuilder {
        app.install(CORS) {
            host("0.0.0.0:${configuration.port}")
        }
        return this
    }
}