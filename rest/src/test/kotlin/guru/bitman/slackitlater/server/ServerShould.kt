package guru.bitman.slackitlater.server

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import guru.bitman.slackitlater.model.SchedulesDeletor
import guru.bitman.slackitlater.server.Main.Companion.serverModule
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.HttpMethod.Companion.Delete
import io.ktor.server.testing.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import java.io.File

class ServerShould {
    private lateinit var messageDeletor: SchedulesDeletor

    @Before
    fun setup() {
        messageDeletor = mock(SchedulesDeletor::class.java)
    }

    @Test
    fun `delete schedule`() = withTestApplication({
        givenDeletionIsScuccessfull()
        givenServer()
    }) {
        with(handleRequest(Delete, SCHEDULE) {
            sendToken()
            sendScheduleToDelete()
        }) {
            thenDeletionSucceeds()
        }
    }

    @Test
    fun `fail to delete schedule`() = withTestApplication({
        givenDeletionFails()
        givenServer()
    }) {
        with(handleRequest(Delete, SCHEDULE) {
            sendToken()
            sendScheduleToDelete()
        }) {
            thenDeletionFails()
        }
    }

    private fun TestApplicationCall.thenDeletionSucceeds() {
        assertEquals(HttpStatusCode.OK, response.status())
    }

    private fun TestApplicationCall.thenDeletionFails() {
        assertEquals(HttpStatusCode.InternalServerError, response.status())
    }

    private fun givenDeletionFails() {
        failDeletion()
    }

    private fun givenDeletionIsScuccessfull() {
        failDeletion()
        succeedDeletionOnSpecificMessage()
    }

    private fun succeedDeletionOnSpecificMessage() {
        doNothing().`when`(messageDeletor).invoke(contains(CHANNEL), contains(MESSAGE_ID), contains(SIL_TOKEN))
    }

    private fun failDeletion() {
        doThrow(RuntimeException()).`when`(messageDeletor).invoke(anyString(), anyString(), anyString())
    }

    private fun Application.givenServer() {
        serverModule(messageDeletor, Configuration(PORT_DOESNT_CARE, FILE_DOESNT_CARE))
    }

    private fun TestApplicationRequest.sendScheduleToDelete() {
        addHeader(HttpHeaders.ContentType, JSON)
        setBody(createBody())
    }

    private fun createBody(): String {
        val mapper = jacksonObjectMapper()
        return mapper.writeValueAsString(DeleteSchedule(CHANNEL, MESSAGE_ID))
    }

    private fun TestApplicationRequest.sendToken() {
        addHeader(SIL_TOKEN_HEADER, SIL_TOKEN)
    }

    private companion object {
        val JSON = ContentType.Application.Json.toString()
        const val SCHEDULE = "/schedule"
        const val SIL_TOKEN_HEADER = "Sil-Token"
        const val SIL_TOKEN = "some-token"
        const val CHANNEL = "this-channel"
        const val MESSAGE_ID = "this-msg-id"
        const val PORT_DOESNT_CARE = ""
        val FILE_DOESNT_CARE = File("")
    }
}