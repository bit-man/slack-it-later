import org.gradle.jvm.tasks.Jar

plugins {
    kotlin("jvm") version "1.3.72"
}

version = "0.1.0"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    compile(project(":core"))
    implementation(kotlin("stdlib-jdk8"))
    implementation("io.ktor:ktor-server-core:1.3.2")
    implementation("io.ktor:ktor-server-netty:1.3.2")
    implementation("io.ktor:ktor-jackson:1.3.2")
    compile("ch.qos.logback:logback-classic:1.2.3")
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.0")
    testImplementation("io.ktor:ktor-server-test-host:1.4.0")
    testImplementation("org.mockito:mockito-core:2.+")
}

val fatJar = task("fatJar", type = Jar::class) {
    baseName = "${project.name}-fat"
    manifest {
        attributes["Implementation-Title"] = "Slack-it-later web server"
        attributes["Implementation-Version"] = version
        attributes["Main-Class"] = "guru.bitman.slackitlater.server.Main"
    }
    from(configurations.runtimeClasspath.get().map({ if (it.isDirectory) it else zipTree(it) }))
    with(tasks.jar.get() as CopySpec)
}


val copyWebResources = task("copyWebResources", type = Copy::class) {
    from("$buildDir/../../web/build/distributions/index.html", "$buildDir/../../web/build/distributions/web.js" )
    into("$buildDir/resources/main")
}


tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    "build" {
        dependsOn(copyWebResources)
        dependsOn(fatJar)
    }
}