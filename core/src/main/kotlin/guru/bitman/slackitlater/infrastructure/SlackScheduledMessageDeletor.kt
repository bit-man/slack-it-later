package guru.bitman.slackitlater.infrastructure

import com.slack.api.Slack
import com.slack.api.methods.MethodsClient
import com.slack.api.methods.request.chat.ChatDeleteScheduledMessageRequest
import guru.bitman.slackitlater.model.SchedulesDeletor

class SlackScheduledMessageDeletor : SchedulesDeletor {
    override fun invoke(channel: String, msgId: String, token: String?) {
        val slack = Slack.getInstance()
        val methods: MethodsClient = slack.methods(token)
        val request = ChatDeleteScheduledMessageRequest.builder()
                .channel(channel)
                .scheduledMessageId(msgId)
                .build()
        val response = methods.chatDeleteScheduledMessage(request)
        if (!response.isOk)
            throw RuntimeException(response.error)
    }

}