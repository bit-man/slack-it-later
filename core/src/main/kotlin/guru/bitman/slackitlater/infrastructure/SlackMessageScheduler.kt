package guru.bitman.slackitlater.infrastructure

import com.slack.api.Slack
import com.slack.api.methods.MethodsClient
import com.slack.api.methods.request.chat.ChatScheduleMessageRequest
import guru.bitman.slackitlater.model.MessageScheduler
import java.util.*


class SlackMessageScheduler(private val token: String?) : MessageScheduler {
    override fun invoke(channel: String, message: String, date: Date) {
        val slack = Slack.getInstance()
        val methods: MethodsClient = slack.methods(token)
        val epoch = date.toInstant().epochSecond
        val request = ChatScheduleMessageRequest.builder()
                .channel(channel)
                .asUser(true)
                .text(message)
                .postAt(epoch.toInt())
                .unfurlLinks(true)
                .unfurlMedia(true)
                .build()
        val response = methods.chatScheduleMessage(request)
        if (!response.isOk)
            throw RuntimeException(response.error)
    }
}