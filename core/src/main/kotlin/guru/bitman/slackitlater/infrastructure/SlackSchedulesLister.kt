package guru.bitman.slackitlater.infrastructure

import com.slack.api.Slack
import com.slack.api.methods.MethodsClient
import com.slack.api.methods.request.chat.scheduled_messages.ChatScheduledMessagesListRequest
import com.slack.api.methods.request.conversations.ConversationsInfoRequest
import com.slack.api.methods.response.chat.scheduled_messages.ChatScheduledMessagesListResponse
import guru.bitman.slackitlater.model.Schedule
import guru.bitman.slackitlater.model.SchedulesLister
import java.util.*

class SlackSchedulesLister(private val token: String?) : SchedulesLister {
    override fun invoke(): List<Schedule> {
        val slack = Slack.getInstance()
        val methods: MethodsClient = slack.methods(token)
        val request = ChatScheduledMessagesListRequest.builder()
                .build()
        val response = methods.chatScheduledMessagesList(request)
        return if (response.isOk)
            response.scheduledMessages.map { toSchedule(it, methods) }.toList()
        else
            throw RuntimeException(response.error)
    }

    private fun toSchedule(it: ChatScheduledMessagesListResponse.ScheduledMessage, methods: MethodsClient): Schedule {
        val epoch = it.postAt.toLong()
        val channelName = retrieveChannelName(it.channelId, methods)
        val messageSummary = textSpy(it.text)
        val postAt = Date(epoch * 1000)
        return Schedule(it.id, channelName, messageSummary, postAt)
    }

    private fun textSpy(text: String) =
            if (text.length <= 100) text else text.substring(0, 100)

    private fun retrieveChannelName(channelId: String, methods: MethodsClient): String {
        val request = ConversationsInfoRequest.builder()
                .channel(channelId)
                .build()
        val response = methods.conversationsInfo(request)
        return if (response.isOk) {
            response.channel.name ?: "unknown, but channel id is ${channelId}"
        } else
            "error looking for channel name (id: ${channelId}, error: ${response.error}"
    }
}
