package guru.bitman.slackitlater.actions

import guru.bitman.slackitlater.model.SchedulesDeletor

class DeleteScheduleMessage(private val driver: SchedulesDeletor) {
    operator fun invoke(channel: String, msgId: String, token: String?) {
        driver.invoke(channel, msgId, token)
    }

}