package guru.bitman.slackitlater.actions

import guru.bitman.slackitlater.model.MessageScheduler
import java.util.*

class ScheduleMessage(private val driver: MessageScheduler) {
    operator fun invoke(channel: String, message: String, date: Date) =
            driver(channel, message, date)
}
