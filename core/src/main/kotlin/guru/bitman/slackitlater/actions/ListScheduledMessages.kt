package guru.bitman.slackitlater.actions

import guru.bitman.slackitlater.model.Schedule
import guru.bitman.slackitlater.model.SchedulesLister

class ListScheduledMessages(private val driver: SchedulesLister) {
    operator fun invoke(): List<Schedule> = driver()
}