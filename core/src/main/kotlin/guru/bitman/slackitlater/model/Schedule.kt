package guru.bitman.slackitlater.model

import java.util.*

data class Schedule(val id: String, val channelName: String, val textSpy: String, val postAt: Date)
