package guru.bitman.slackitlater.model

interface SchedulesDeletor {
    fun invoke(channel: String, msgId: String, token: String?)
}
