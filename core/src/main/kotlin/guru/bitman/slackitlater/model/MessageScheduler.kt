package guru.bitman.slackitlater.model

import java.util.*

interface MessageScheduler {
    operator fun invoke(channel: String, message: String, date: Date)
}
