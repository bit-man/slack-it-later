package guru.bitman.slackitlater.model

interface SchedulesLister {
    operator fun invoke(): List<Schedule>
}
