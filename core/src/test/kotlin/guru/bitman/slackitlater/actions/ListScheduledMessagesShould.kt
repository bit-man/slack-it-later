package guru.bitman.slackitlater.actions

import guru.bitman.slackitlater.model.SchedulesLister
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.*

class ListScheduledMessagesShould() {

    private lateinit var schedule: ListScheduledMessages
    private lateinit var driverSchedule: SchedulesLister

    @BeforeEach
    fun setup() {
        driverSchedule = mock(SchedulesLister::class.java)
        schedule = ListScheduledMessages(driverSchedule)
    }

    @Test
    fun `invoke driver`() {
        schedule.invoke()

        verify(driverSchedule, times(1)).invoke()
    }

    @Test
    fun `fail when driver fails`() {

        doThrow(RuntimeException::class.java).`when`(driverSchedule).invoke()

        val executable = { val invoke = schedule.invoke() }

        assertThrows<RuntimeException>("Should throw Exception", executable)
    }
}