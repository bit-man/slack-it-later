package guru.bitman.slackitlater.actions

import guru.bitman.slackitlater.model.SchedulesDeletor
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito

class DeleteScheduleMessageShould {
    private lateinit var schedule: DeleteScheduleMessage
    private lateinit var driverSchedule: SchedulesDeletor

    @BeforeEach
    fun setup() {
        driverSchedule = Mockito.mock(SchedulesDeletor::class.java)
        schedule = DeleteScheduleMessage(driverSchedule)
    }

    @Test
    fun `invoke driver`() {
        val messageId = ""
        val channel = ""
        schedule.invoke(channel, messageId, TOKEN)

        Mockito.verify(driverSchedule, Mockito.times(1)).invoke(channel, messageId, TOKEN)
    }

    @Test
    fun `fail when driver fails`() {

        val messageId = ""
        val channel = ""
        Mockito.doThrow(RuntimeException::class.java).`when`(driverSchedule).invoke(channel, messageId, TOKEN)

        val executable = { schedule.invoke(channel, messageId, TOKEN) }

        assertThrows<RuntimeException>("Should throw Exception", executable)
    }

    private companion object {
        const val TOKEN = "some-token"
    }
}