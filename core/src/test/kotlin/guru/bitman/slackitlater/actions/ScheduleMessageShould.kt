package guru.bitman.slackitlater.actions

import guru.bitman.slackitlater.model.MessageScheduler
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.*
import java.util.*

class ScheduleMessageShould() {

    private lateinit var schedule: ScheduleMessage
    private lateinit var driverSchedule: MessageScheduler

    @BeforeEach
    fun setup() {
        driverSchedule = mock(MessageScheduler::class.java)
        schedule = ScheduleMessage(driverSchedule)
    }

    @Test
    fun `invoke driver`() {
        val channel = "some_channel"
        val message = "some message"
        val date = Date()

        schedule.invoke(channel, message, date)

        verify(driverSchedule, times(1)).invoke(channel, message, date)
    }

    @Test
    fun `fail when driver fails`() {
        val channel = "some_channel"
        val message = "some message"
        val date = Date()

        doThrow(RuntimeException::class.java).`when`(driverSchedule).invoke(channel, message, date)

        val executable = { schedule.invoke(channel, message, date) }

        assertThrows<RuntimeException>("Should throw Exception", executable)
    }
}