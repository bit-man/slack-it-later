import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.72"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("com.slack.api:slack-api-client:1.0.2")
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.0")
    testImplementation("org.mockito:mockito-core:2.+")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
}


tasks.withType<Test> {
    useJUnitPlatform()
}
