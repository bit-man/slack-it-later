package guru.bitman.slackitlater.web.dashboard

data class NewSchedule(val channel: String, val message: String, val postAt: Double)
