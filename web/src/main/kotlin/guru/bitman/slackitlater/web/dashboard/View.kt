package guru.bitman.slackitlater.web.dashboard

import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.DELETE_SCHEDULED_MESSAGES_BUTTON_ID
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.NEW_SCHEDULE_BUTTON_ID
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.NEW_SCHEDULE_CHANNEL
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.NEW_SCHEDULE_MESSAGE
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.NEW_SCHEDULE_TIMESTAMP
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.REFRESH_BUTTON_ID
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.TOKEN_BUTTON_ID
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.TOKEN_ID
import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.getScheduleElementCheckboxIdFor
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.events.Event
import org.w3c.dom.events.MouseEvent
import kotlin.browser.document
import kotlin.browser.window
import kotlin.js.Date

class View(private val presenter: Presenter, private val scheduledMessagesContainer: ScheduledMessagesContainer) {
    fun run() {
        tokenButtonSetup()
        refreshButtonSetup()
        newScheduleSetup()
        deleteScheduleSetup()
        subscribeToScheduledMessages()
    }

    private fun deleteScheduleSetup() {
        val deleteSchedulesButton = document.getElementById(DELETE_SCHEDULED_MESSAGES_BUTTON_ID) as HTMLButtonElement
        deleteSchedulesButton.onclick = deleteScheduleButtonPressed()
    }

    private fun newScheduleSetup() {
        val newScheduleButton = document.getElementById(NEW_SCHEDULE_BUTTON_ID) as HTMLButtonElement
        newScheduleButton.onclick = newScheduleButtonPressed()
    }

    private fun refreshButtonSetup() {
        val refreshButton = document.getElementById(REFRESH_BUTTON_ID) as HTMLButtonElement
        refreshButton.onclick = refreshButtonPressed()
    }

    private fun deleteScheduleButtonPressed(): (Event) -> Unit = {
        presenter.deleteSchedulesButtonPressed()
    }

    private fun newScheduleButtonPressed(): (Event) -> Unit =
            { _ ->
                val channel = document.getElementById(NEW_SCHEDULE_CHANNEL) as HTMLInputElement
                val message = document.getElementById(NEW_SCHEDULE_MESSAGE) as HTMLInputElement
                val date = document.getElementById(NEW_SCHEDULE_TIMESTAMP) as HTMLInputElement
                val postAt = Date.parse(date.value)
                val schedule = NewSchedule(channel.value, message.value, postAt)
                presenter.newScheduleButtonPressed(schedule)
            }

    private fun refreshButtonPressed(): (Event) -> Unit =
            { _ ->
                presenter.refreshButtonPressed()
            }

    private fun subscribeToScheduledMessages() {
        presenter.subscribeToScheduledMessages { schedules -> fillScheduledMessagesContainer(schedules) }
    }

    private fun fillScheduledMessagesContainer(schedules: List<Schedule>) {
        scheduledMessagesContainer.replace(schedules)
        schedules.forEach {
            handleScheduleToDelete(it)
        }
    }

    private fun handleScheduleToDelete(schedule: Schedule) {
        val checkBox = document.getElementById(getScheduleElementCheckboxIdFor(schedule)) as HTMLInputElement
        checkBox.onclick = addScheduleToDelete(checkBox, schedule)
    }

    private fun addScheduleToDelete(checkBox: HTMLInputElement, schedule: Schedule): (MouseEvent) -> Unit {
        return {
            val candidate = DeleteSchedule(schedule.channelName, schedule.id)
            if (checkBox.checked) {
                presenter.addScheduleToDelete(candidate)
            } else {
                presenter.removeScheduleFromDeletion(candidate)
            }
        }
    }

    private fun tokenButtonSetup() {
        reactToTokenButtonPressed()
        subscribeToInvalidTokenError()
        subscribeToValidToken()
    }

    private fun subscribeToValidToken() {
        presenter.subscribeToValidToken {
            val scheduledMessages = presenter.retrieveScheduledMessages()
            hideButton(TOKEN_BUTTON_ID)
            showButton(REFRESH_BUTTON_ID)
            fillScheduledMessagesContainer(scheduledMessages)
        }
    }

    private fun reactToTokenButtonPressed() {
        val tokenButton = document.getElementById(TOKEN_BUTTON_ID) as HTMLButtonElement
        tokenButton.onclick = tokenButtonPressed()
    }

    private fun subscribeToInvalidTokenError() {
        presenter.subscribeToInvalidToken { showErrorDialog() }
    }

    private fun tokenButtonPressed(): (Event) -> Unit =
            { _ ->
                val tokenInput = document.getElementById(TOKEN_ID) as HTMLInputElement
                presenter.tokenButtonPressed(tokenInput.value)
            }

    private fun showErrorDialog() {
        window.alert("Invalid token")
        showButton(TOKEN_BUTTON_ID)
        hideButton(REFRESH_BUTTON_ID)
        scheduledMessagesContainer.clean()
    }

    private fun showButton(id: String) {
        hideButton(id, false)
    }

    private fun hideButton(id: String) {
        hideButton(id, true)
    }

    private fun hideButton(id: String, hiddenStatus: Boolean) {
        val tokenButton = document.getElementById(id) as HTMLButtonElement
        tokenButton.hidden = hiddenStatus
    }
}
