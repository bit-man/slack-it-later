package guru.bitman.slackitlater.web

import guru.bitman.slackitlater.web.dashboard.*
import kotlin.browser.document

fun main() {
    val factory = ElementFactory()
    factory.createDashboard()
    val serverUrl = createServerURL()
    val configuration = Configuration(serverUrl)
    val presenter = Presenter(ApiModel(configuration = configuration))
    val messagesRepository = DOMScheduledMessagesRepository(factory, InMemoryScheduledMessagesRepository())
    val scheduledMessagesContainer = ScheduledMessagesContainer(messagesRepository)
    View(presenter, scheduledMessagesContainer).run()
}

private fun createServerURL(): String {
    val protocol = document.location?.protocol
    val host = document.location?.host
    return "$protocol//$host"
}