package guru.bitman.slackitlater.web.dashboard

import kotlin.js.Date

data class Schedule(val id: String, val channelName: String, val textSpy: String, val postAt: Date)
