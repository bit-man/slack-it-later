package guru.bitman.slackitlater.web.dashboard

interface ScheduledMessagesRepository {
    fun addAll(schedules: List<Schedule>)
    fun containsById(id: String): Boolean
    fun clean()
}
