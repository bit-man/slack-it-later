package guru.bitman.slackitlater.web.dashboard

import kotlin.properties.Delegates


class Presenter(private val model: Model) {
    private val schedulesToDelete: MutableList<DeleteSchedule> = mutableListOf()
    private val scheduledMessagesNotification: MutableList<(List<Schedule>) -> Unit> = mutableListOf()
    private var invalidTokenNotification: MutableList<() -> Unit> = mutableListOf()
    private var validTokenNotification: MutableList<() -> Unit> = mutableListOf()
    private var token: String by Delegates.observable("") { _, _, newValue ->
        notifyOnToken(newValue)
    }

    private fun notifyOnToken(newValue: String) {
        if (newValue.isValid()) {
            notifyValidToken()
        } else {
            notifyInvalidToken()
        }
    }

    private fun notifyScheduledMessages() {
        val schedules = retrieveScheduledMessages()
        scheduledMessagesNotification.forEach {
            it(schedules)
        }
    }

    private fun notifyInvalidToken() {
        invalidTokenNotification.forEach { it() }
    }

    private fun notifyValidToken() {
        validTokenNotification.forEach { it() }
    }

    fun retrieveScheduledMessages(): List<Schedule> {
        return model.retrieveSchedules(token)
    }

    fun subscribeToInvalidToken(function: () -> Unit) {
        invalidTokenNotification.add(function)
    }

    fun subscribeToValidToken(function: () -> Unit) {
        validTokenNotification.add(function)
    }

    fun tokenButtonPressed(value: String?) {
        token = value ?: ""
    }

    fun subscribeToScheduledMessages(function: (List<Schedule>) -> Unit) {
        scheduledMessagesNotification.add(function)
    }

    fun refreshButtonPressed() {
        if (token.isValid())
            notifyScheduledMessages()
        else
            notifyInvalidToken()
    }

    fun newScheduleButtonPressed(newSchedule: NewSchedule) {
        model.submitSchedule(newSchedule, token)
        if (token.isValid())
            notifyScheduledMessages()
        else
            notifyInvalidToken()
    }

    fun deleteSchedulesButtonPressed() {
        if (schedulesToDelete.isEmpty()) return
        if (!token.isValid()) {
            notifyInvalidToken()
            return
        }

        model.deleteSchedules(schedulesToDelete, token)
        schedulesToDelete.clear()
        notifyScheduledMessages()
    }

    fun addScheduleToDelete(schedule: DeleteSchedule) {
        schedulesToDelete.add(schedule)
    }

    fun removeScheduleFromDeletion(schedule: DeleteSchedule) {
        schedulesToDelete.remove(schedule)
    }

}

private fun String.isValid() = !this.isBlank()


