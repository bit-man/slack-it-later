package guru.bitman.slackitlater.web.dashboard

class ScheduledMessagesContainer(private val repository: ScheduledMessagesRepository) {
    fun replace(schedules: List<Schedule>) {
        repository.clean()
        repository.addAll(schedules)
    }

    fun clean() {
        repository.clean()
    }
}
