package guru.bitman.slackitlater.web.dashboard

interface Model {
    fun retrieveSchedules(token: String): List<Schedule>
    fun submitSchedule(newSchedule: NewSchedule, token: String)
    fun deleteSchedules(deleteSchedules: List<DeleteSchedule>, token: String)
}
