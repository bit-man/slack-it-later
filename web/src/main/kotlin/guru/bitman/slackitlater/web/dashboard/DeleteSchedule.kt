package guru.bitman.slackitlater.web.dashboard

data class DeleteSchedule(val channel: String, val messageId: String)
