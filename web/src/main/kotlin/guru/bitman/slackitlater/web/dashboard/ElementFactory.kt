package guru.bitman.slackitlater.web.dashboard

import kotlinx.html.*
import kotlinx.html.dom.create
import org.w3c.dom.HTMLElement
import org.w3c.dom.Node
import kotlin.browser.document

class ElementFactory {
    fun createDashboard() {
        val tokenContainer = createTokenContainer()
        val newScheduleContainer = createNewScheduleContainer()
        val deleteScheduleContainer = createDeleteScheduleContainer()
        val schedulesContainer = createSchedulesContainer()

        document.body!!.append(tokenContainer)
        document.body!!.append(newScheduleContainer)
        document.body!!.append(deleteScheduleContainer)
        document.body!!.append(schedulesContainer)
    }

    private fun createDeleteScheduleContainer(): HTMLElement {
        return document.create.div {
            id = DELETE_SCHEDULED_MESSAGES_CONTAINER_ID
            button {
                id = DELETE_SCHEDULED_MESSAGES_BUTTON_ID
                +"Delete marked schedules"
            }
        }
    }

    private fun createSchedulesContainer(): HTMLElement {
        return document.create.div {
            id = SCHEDULED_MESSAGES_CONTAINER_ID
        }
    }

    private fun createNewScheduleContainer(): HTMLElement {
        return document.create.div {
            id = NEW_SCHEDULE_CONTAINER_ID
            div {
                strong {
                    +"message"
                }
                input {
                    id = NEW_SCHEDULE_MESSAGE
                    type = InputType.text
                }
            }
            div {
                strong {
                    +"channel"
                }
                input {
                    id = NEW_SCHEDULE_CHANNEL
                    type = InputType.text
                }
            }
            div {
                strong {
                    +"timestamp"
                }
                input {
                    id = NEW_SCHEDULE_TIMESTAMP
                    type = InputType.text
                }
                p {
                    +NEW_SCHEDULE_TIMESTAMP_FORMAT_HELP
                }
            }
            button {
                id = NEW_SCHEDULE_BUTTON_ID
                +"new schedule"
            }
        }
    }

    private fun createTokenContainer(): HTMLElement {
        return document.create.div {
            id = TOKEN_CONTAINER_ID
            input {
                id = TOKEN_ID
                type = InputType.text
            }
            button {
                id = TOKEN_ACTIVATION_BUTTON_ID
                +TOKEN_ACTIVATION_BUTTON_DISPLAY_MESSAGE
            }
            button {
                id = REFRESH_BUTTON_ID
                hidden = true
                +REFRESH_BUTTON_MESSAGE
            }
        }
    }

    fun createScheduleElementFrom(schedule: Schedule): Node {
        return document.create.div {
            id = "schedule-${schedule.id}"
            div {
                id = "schedule-${schedule.id}-channel-name"
                checkBoxInput {
                    checked = false
                    id = getScheduleElementCheckboxIdFor(schedule)
                }
                strong {
                    +"Channel: "
                }
                +schedule.channelName
            }
            div {
                id = "schedule-${schedule.id}-text"
                strong {
                    +"Text: "
                }
                +schedule.textSpy
            }
            div {
                id = "schedule-${schedule.id}-post-time"
                strong {
                    +"Posted at: "
                }
                +"${schedule.postAt.toLocaleDateString()} ${schedule.postAt.toLocaleTimeString()}"
            }
            hr { }
        }
    }

    companion object {
        fun getScheduleElementCheckboxIdFor(scheduleId: Schedule) =
                "schedule-${scheduleId.id}-delete"

        const val SCHEDULED_MESSAGES_CONTAINER_ID = "scheduled-messages-container"
        const val NEW_SCHEDULE_CONTAINER_ID = "new-schedule-container"
        const val TOKEN_CONTAINER_ID = "token-container"
        const val TOKEN_BUTTON_ID = "token-button"
        const val TOKEN_ID = "token-text"
        const val TOKEN_ACTIVATION_BUTTON_ID = "token-button"
        const val TOKEN_ACTIVATION_BUTTON_DISPLAY_MESSAGE = "Activate token"
        const val REFRESH_BUTTON_ID = "refresh-button"
        const val REFRESH_BUTTON_MESSAGE = "Refresh"
        const val NEW_SCHEDULE_BUTTON_ID = "new-schedule-button"
        const val NEW_SCHEDULE_MESSAGE = "new-schedule-message"
        const val NEW_SCHEDULE_CHANNEL = "new-schedule-channel"
        const val NEW_SCHEDULE_TIMESTAMP = "new-schedule-date"
        const val NEW_SCHEDULE_TIMESTAMP_FORMAT_HELP = "Format (e.g.): 17 Aug 2020 10:00"
        const val DELETE_SCHEDULED_MESSAGES_CONTAINER_ID = "delete-schedules-container"
        const val DELETE_SCHEDULED_MESSAGES_BUTTON_ID = "delete-schedules-button"
    }

}
