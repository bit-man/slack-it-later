package guru.bitman.slackitlater.web.dashboard

import guru.bitman.slackitlater.web.dashboard.ElementFactory.Companion.SCHEDULED_MESSAGES_CONTAINER_ID
import org.w3c.dom.HTMLDivElement
import kotlin.browser.document
import kotlin.dom.clear

class DOMScheduledMessagesRepository(private val factory: ElementFactory, private val scheduledMessagesRepository: InMemoryScheduledMessagesRepository) : ScheduledMessagesRepository {
    private val container = document.getElementById(SCHEDULED_MESSAGES_CONTAINER_ID) as HTMLDivElement

    override fun addAll(schedules: List<Schedule>) {
        clean()
        addToContainer(schedules)
    }

    private fun addToContainer(schedules: List<Schedule>) {
        schedules.forEach {
            container.appendChild(factory.createScheduleElementFrom(it))
        }
        scheduledMessagesRepository.addAll(schedules)
    }

    override fun containsById(id: String): Boolean {
        return scheduledMessagesRepository.schedules().filter { it.id == id }.any()
    }

    override fun clean() {
        scheduledMessagesRepository.clean()
        container.clear()
    }

}
