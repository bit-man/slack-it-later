package guru.bitman.slackitlater.web.dashboard

import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.list
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import org.w3c.xhr.XMLHttpRequest
import kotlin.js.Date

class ApiModel(private val configuration: Configuration) : Model {
    @ExperimentalStdlibApi
    override fun retrieveSchedules(token: String): List<Schedule> {
        val request: dynamic = sendSchedulesRequest(getScheduleRequestURL(), SCHEDULES_REQUEST_METHOD, token)
        val response = parseSchedules(request)
        return response.map { toModel(it) }
    }

    override fun submitSchedule(newSchedule: NewSchedule, token: String) {
        val request: dynamic = sendRequestWithBody(getNewScheduleRequestURL(), NEW_SCHEDULE_REQUEST_METHOD, token, newSchedule)
        if (scheduleNotCreated(request)) {
            fail()
        }
    }

    override fun deleteSchedules(deleteSchedules: List<DeleteSchedule>, token: String) {
        deleteSchedules.forEach {
            val request: dynamic = sendRequestWithBody(getDeleteScheduleRequestURL(), DELETE_SCHEDULE_REQUEST_METHOD, token, it)
            if (scheduleDeletionFails(request)) {
                fail()
            }
        }
    }

    private fun getDeleteScheduleRequestURL() = configuration.serverUrl + DELETE_SCHEDULE_REQUEST_DIRECTORY

    private fun scheduleDeletionFails(request: dynamic) = request.status != OK_STATUS

    private fun fail() {
        throw RuntimeException()
    }

    private fun scheduleNotCreated(request: dynamic) = request.status != CREATED_STATUS

    private fun parseSchedules(request: dynamic): List<SingleScheduleResponse> {
        val json = Json(JsonConfiguration.Stable)
        return json.parse(SingleScheduleResponse.serializer().list, request.responseText)
    }

    private fun sendSchedulesRequest(url: String, method: String, token: String): dynamic {
        val request: dynamic = createRequest(method, url, token)
        request.send()
        return request
    }

    private fun sendRequestWithBody(url: String, method: String, token: String, body: Any): dynamic {
        val request: dynamic = createRequest(method, url, token)
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
        request.send(JSON.stringify(body))
        return request
    }

    private fun createRequest(method: String, url: String, token: String): dynamic {
        val request: dynamic = XMLHttpRequest()
        request.open(method, url, REQUEST_IS_SYNC)
        request.setRequestHeader(TOKEN_HEADER, token)
        return request
    }

    private fun toModel(response: SingleScheduleResponse) = Schedule(response.id, response.channelName, response.textSpy, Date(response.postAt))

    private fun getScheduleRequestURL() = configuration.serverUrl + SCHEDULES_REQUEST_SUBDIRECTORY

    private fun getNewScheduleRequestURL() = configuration.serverUrl + NEW_SCHEDULE_REQUEST_DIRECTORY

    private companion object {
        const val REQUEST_IS_SYNC = false
        const val SCHEDULES_REQUEST_METHOD = "GET"
        const val SCHEDULES_REQUEST_SUBDIRECTORY = "/schedules"
        const val NEW_SCHEDULE_REQUEST_METHOD = "POST"
        const val NEW_SCHEDULE_REQUEST_DIRECTORY = "/schedule"
        const val TOKEN_HEADER = "Sil-Token"
        const val CREATED_STATUS = 201
        const val OK_STATUS = 200
        const val DELETE_SCHEDULE_REQUEST_METHOD = "DELETE"
        const val DELETE_SCHEDULE_REQUEST_DIRECTORY = NEW_SCHEDULE_REQUEST_DIRECTORY
    }
}

@Serializable
data class SingleScheduleResponse(val id: String, val channelName: String, val textSpy: String, val postAt: Float)
