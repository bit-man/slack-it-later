package guru.bitman.slackitlater.web.dashboard

data class Configuration(val serverUrl: String)
