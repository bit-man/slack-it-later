package guru.bitman.slackitlater.web.dashboard

class InMemoryScheduledMessagesRepository : ScheduledMessagesRepository {
    private val schedules: MutableList<Schedule> = ArrayList()

    fun schedules(): Collection<Schedule> {
        return schedules
    }

    override fun addAll(schedules: List<Schedule>) {
        this.schedules.addAll(schedules)
    }

    override fun containsById(id: String) =
            schedules.filter { it.id == id }.any()

    override fun clean() {
        schedules.clear()
    }

}
