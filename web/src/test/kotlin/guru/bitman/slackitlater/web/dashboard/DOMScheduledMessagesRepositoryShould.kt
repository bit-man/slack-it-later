package guru.bitman.slackitlater.web.dashboard

import kotlin.js.Date
import kotlin.test.*

class DOMScheduledMessagesRepositoryShould {
    private lateinit var schedules: InMemoryScheduledMessagesRepository
    private lateinit var repository: DOMScheduledMessagesRepository

    @BeforeTest
    fun setup() {
        schedules = InMemoryScheduledMessagesRepository()
    }

    @Test
    fun clean() {
        givenRepository()

        repository.clean()

        assertTrue(schedules.schedules().isEmpty())
    }

    @Test
    fun addsAllElementsLocatedByContainsById() {
        givenRepository()
        val schedule = listOf(SCHEDULE_1, SCHEDULE_2)
        repository.addAll(schedule)

        assertEquals(2, schedules.schedules().size)
        assertTrue(repository.containsById("1"))
        assertTrue(repository.containsById("2"))
    }

    @Test
    fun addsAllElementsAndCleansExistingLocatedByContainsById() {
        givenRepository()
        val schedulePre = listOf(SCHEDULE_3)
        repository.addAll(schedulePre)

        val schedule = listOf(SCHEDULE_1, SCHEDULE_2)
        repository.addAll(schedule)

        assertEquals(2, schedules.schedules().size)
        assertTrue(repository.containsById("1"))
        assertTrue(repository.containsById("2"))
        assertFalse(repository.containsById("3"))
    }

    @Test
    fun findAddedElementLocatedByContainsById() {
        givenRepository()
        val schedule = listOf(SCHEDULE_1)
        repository.addAll(schedule)

        val containsAddedSchedule = repository.containsById("1")

        assertTrue(containsAddedSchedule)
    }

    @Test
    fun addElementsToSchedules() {
        givenRepository()
        val schedule = listOf(SCHEDULE_1, SCHEDULE_3)
        repository.addAll(schedule)

        val schedules = schedules.schedules()

        assertEquals(2, schedules.size)
        assertTrue(schedules.contains(SCHEDULE_1))
        assertTrue(schedules.contains(SCHEDULE_3))
    }

    private fun givenRepository() {
        repository = DOMScheduledMessagesRepository(ElementFactory(), schedules)
    }

    private companion object {
        val POST_DATE = Date()
        val SCHEDULE_1 = Schedule("1", "channel-01", "", POST_DATE)
        val SCHEDULE_2 = Schedule("2", "channel-02", "", POST_DATE)
        val SCHEDULE_3 = Schedule("3", "channel-03", "", POST_DATE)
    }
}