package guru.bitman.slackitlater.web.dashboard

import kotlin.js.Date
import kotlin.test.*

private class PresenterShould {
    private var validTokenSubscriptionInvoked = false
    private lateinit var model: TestModel
    private lateinit var schedules: List<Schedule>
    private var scheduleMessagesUpdated: Boolean = false
    private lateinit var presenter: Presenter
    private var error: Boolean = false

    @BeforeTest
    fun setup() {
        val testSchedules = mutableListOf(Schedule("1", "", "", POST_TIME), Schedule("2", "", "", POST_TIME))
        model = TestModel(testSchedules)
        presenter = Presenter(model)
        presenter.subscribeToInvalidToken { this.error = true }
        presenter.subscribeToValidToken {
            this.error = false
            this.validTokenSubscriptionInvoked = true
        }
        presenter.subscribeToScheduledMessages { schedules ->
            this.scheduleMessagesUpdated = true
            this.schedules = schedules
        }
    }

    @Test
    fun savingNonEmptyTokenDoesNotThrowError() {
        givenValidToken()

        assertFalse(error)
        thenValidTokenSubscriptionWasInvoked()
    }

    @Test
    fun savingNullTokenThrowsError() {
        givenToken(null)

        thenErrorIsNotified()
        thenValidTokenSubscriptionWasNotInvoked()
    }

    @Test
    fun savingEmptyTokenThrowsError() {
        givenToken("")

        thenErrorIsNotified()
        thenValidTokenSubscriptionWasNotInvoked()
    }

    @Test
    fun savingNullTokenWontUpdateScheduledMessages() {
        givenToken("")

        assertFalse(scheduleMessagesUpdated)
    }

    @Test
    fun savingEmptyTokenWontUpdateScheduledMessages() {
        givenToken("")

        assertFalse(scheduleMessagesUpdated)
    }

    @Test
    fun retrieveSchedulesWhenRefreshButtonPressed() {
        givenValidToken()
        presenter.refreshButtonPressed()

        thenUpdateSchedules()
        assertEquals(2, schedules.size)
        assertTrue(schedules.contains(Schedule("1", "", "", POST_TIME)))
        assertTrue(schedules.contains(Schedule("2", "", "", POST_TIME)))
    }

    @Test
    fun doesNotInvokeValidTokenSubscriptionWhenRefreshButtonPressed() {
        givenValidToken()
        givenValidTokenNotificationIsReset()
        presenter.refreshButtonPressed()

        thenValidTokenSubscriptionWasNotInvoked()
    }

    @Test
    fun failWhenRefreshButtonPressedAndInvalidToken() {
        givenToken(null)
        presenter.refreshButtonPressed()

        assertFalse(scheduleMessagesUpdated)
    }

    @Test
    fun notifyErrorWhenNewScheduleOnInvalidKey() {
        presenter.newScheduleButtonPressed(NewSchedule("channel", "message", ANY_DATE_EPOCH))

        thenErrorIsNotified()
    }

    @Test
    fun updateSchedulesWhenNewSchedule() {
        givenValidToken()

        presenter.newScheduleButtonPressed(NewSchedule("channel", "message", ANY_DATE_EPOCH))

        thenUpdateSchedules()
    }

    @Test
    fun retrieveSchedulesWithNewOneWhenNewSchedule() {
        givenValidToken()
        val timestamp = ANY_DATE_EPOCH
        presenter.newScheduleButtonPressed(NewSchedule("channel", "message", timestamp))
        val anyDate = Date(timestamp)

        assertEquals(3, schedules.size)
        assertTrue(schedules.contains(Schedule("1", "", "", POST_TIME)))
        assertTrue(schedules.contains(Schedule("2", "", "", POST_TIME)))
        thenNewScheduleWasSent(anyDate)
    }

    @Test
    fun retrieveSchedulesWithValidTokenDoesNotInvokeValidTokenSubscription() {
        givenValidToken()
        givenValidTokenNotificationIsReset()
        val timestamp = ANY_DATE_EPOCH
        presenter.newScheduleButtonPressed(NewSchedule("channel", "message", timestamp))

        thenValidTokenSubscriptionWasNotInvoked()
    }

    @Test
    fun signalErrorOnDeletionWhenInvalidToken() {
        givenInvalidToken()
        givenErrorStatusIsReset()
        givenScheduleToDelete(DeleteSchedule("1", ""))

        whenDeletingSchedules()

        thenErrorIsNotified()
        thenNoSchedulesWereDeleted()
    }

    @Test
    fun deleteSingleSchedule() {
        givenValidToken()
        givenScheduleToDelete(DeleteSchedule("1", ""))

        whenDeletingSchedules()

        assertEquals(1, schedules.size)
        assertTrue(schedules.contains(Schedule("2", "", "", POST_TIME)))
    }

    @Test
    fun updateScheduledMessagesWhenDeleteSingleSchedule() {
        givenValidToken()
        givenUpdateSchedulesIsReset()
        givenScheduleToDelete(DeleteSchedule("1", ""))

        whenDeletingSchedules()

        thenUpdateSchedules()
    }

    @Test
    fun updateScheduledMessagesWhenDeleteSchedule() {
        givenValidToken()

        whenDeletingSchedules()

        thenCallsToScheduleDeletionWere(0)
    }

    @Test
    fun cleanSchedulesToDeleteAfterSuccessfulDeletion() {
        givenValidToken()
        givenErrorStatusIsReset()
        givenScheduleToDelete(DeleteSchedule("1", ""))
        givenDeletingSchedules()

        whenDeletingSchedules()

        thenCallsToScheduleDeletionWere(1)
    }

    @Test
    fun deleteTwoSchedules() {
        givenValidToken()
        givenScheduleToDelete(DeleteSchedule("1", ""))
        givenScheduleToDelete(DeleteSchedule("2", ""))

        whenDeletingSchedules()

        assertTrue(schedules.isEmpty())
    }

    @Test
    fun notDeleteScheduleWhenRemovedFromDeletion() {
        givenValidToken()
        givenScheduleToDelete(DeleteSchedule("1", ""))
        givenScheduleUnmarkedForDeletion(DeleteSchedule("1", ""))

        whenDeletingSchedules()

        thenCallsToScheduleDeletionWere(0)
    }

    private fun givenScheduleUnmarkedForDeletion(schedule: DeleteSchedule) {
        presenter.removeScheduleFromDeletion(schedule)
    }

    private fun thenUpdateSchedules() {
        assertTrue(scheduleMessagesUpdated)
    }

    private fun givenUpdateSchedulesIsReset() {
        scheduleMessagesUpdated = false
    }

    private fun thenCallsToScheduleDeletionWere(expectedDeletionCalls: Int) {
        assertEquals(expectedDeletionCalls, model.deletionCalls)
    }

    private fun givenDeletingSchedules() {
        deleteSchedules()
    }

    private fun thenNoSchedulesWereDeleted() {
        val retrieveSchedules = model.retrieveSchedules(VALID_TOKEN)
        assertEquals(2, retrieveSchedules.size)
        assertTrue(retrieveSchedules.contains(Schedule("1", "", "", POST_TIME)))
        assertTrue(retrieveSchedules.contains(Schedule("2", "", "", POST_TIME)))
    }

    private fun givenScheduleToDelete(schedule: DeleteSchedule) {
        presenter.addScheduleToDelete(schedule)
    }

    private fun givenErrorStatusIsReset() {
        error = false
    }

    private fun thenErrorIsNotified() {
        assertTrue(error)
    }

    private fun whenDeletingSchedules() {
        deleteSchedules()
    }

    private fun deleteSchedules() {
        presenter.deleteSchedulesButtonPressed()
    }

    private fun thenValidTokenSubscriptionWasInvoked() {
        assertTrue(validTokenSubscriptionInvoked)
    }

    private fun thenValidTokenSubscriptionWasNotInvoked() {
        assertFalse(validTokenSubscriptionInvoked)
    }

    private fun thenNewScheduleWasSent(anyDate: Date) {
        val newSchedule = schedules[2]
        assertEquals(anyDate.getMilliseconds(), newSchedule.postAt.getMilliseconds())
        assertEquals("3", newSchedule.id)
        assertEquals("channel", newSchedule.channelName)
        assertEquals("message", newSchedule.textSpy)
    }

    private fun givenValidToken() {
        givenToken(VALID_TOKEN)
    }

    private fun givenInvalidToken() {
        givenToken(INVALID_TOKEN)
    }

    private fun givenToken(tokenValue: String?) {
        presenter.tokenButtonPressed(tokenValue)
    }

    private fun givenValidTokenNotificationIsReset() {
        validTokenSubscriptionInvoked = false
    }

    private companion object {
        val POST_TIME = Date()
        val ANY_DATE_EPOCH = Date.now()
        const val INVALID_TOKEN = ""
        const val VALID_TOKEN = "valid-token"
    }
}