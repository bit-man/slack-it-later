package guru.bitman.slackitlater.web.dashboard

import kotlin.js.Date
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ScheduledMessagesContainerShould {
    @Test
    fun addSchedules() {
        val repository = InMemoryScheduledMessagesRepository()
        val container = ScheduledMessagesContainer(repository)
        val schedules = listOf(Schedule("id1", "", "", Date()), Schedule("id2", "", "", Date()))

        container.replace(schedules)

        assertEquals(2, repository.schedules().size)
    }

    @Test
    fun addSameSchedulesDeletingPreviousOnes() {
        val repository = InMemoryScheduledMessagesRepository()
        val container = ScheduledMessagesContainer(repository)
        container.replace(listOf(Schedule("id1", "", "", Date())))

        val schedules = listOf(Schedule("id2", "", "", Date()), Schedule("id3", "", "", Date()))
        container.replace(schedules)

        assertEquals(2, repository.schedules().size)
        assertFalse(repository.containsById("id1"))
        assertTrue(repository.containsById("id2"))
        assertTrue(repository.containsById("id3"))
    }

    @Test
    fun cleanContainer() {
        val repository = InMemoryScheduledMessagesRepository()
        val container = ScheduledMessagesContainer(repository)
        container.replace(listOf(Schedule("id1", "", "", Date())))

        container.clean()

        assertTrue(repository.schedules().isEmpty())
    }
}