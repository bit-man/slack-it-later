package guru.bitman.slackitlater.web.dashboard

import kotlin.js.Date

class TestModel(private val schedules: MutableList<Schedule>) : Model {
    var deletionCalls = 0

    override fun retrieveSchedules(token: String): List<Schedule> {
        return schedules
    }

    override fun submitSchedule(newSchedule: NewSchedule, token: String) {
        schedules.add(Schedule("3", newSchedule.channel, newSchedule.message, Date(newSchedule.postAt)))
    }

    override fun deleteSchedules(deleteSchedules: List<DeleteSchedule>, token: String) {
        deletionCalls++
        val removalCandidates = schedules.filter { deleteSchedules.contains(DeleteSchedule(it.id, it.channelName)) }
        schedules.removeAll(removalCandidates)
    }

}
