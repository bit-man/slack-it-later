import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.72"
}

dependencies {
    compile(project(":core"))
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.ocpsoft.prettytime:prettytime-nlp:4.0.4.Final")
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.0")
    testImplementation("org.mockito:mockito-core:2.+")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
}