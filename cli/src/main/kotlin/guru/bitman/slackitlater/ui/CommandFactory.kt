package guru.bitman.slackitlater.ui

import guru.bitman.slackitlater.actions.DeleteScheduleMessage
import guru.bitman.slackitlater.actions.ScheduleMessage
import guru.bitman.slackitlater.infrastructure.SlackMessageScheduler
import guru.bitman.slackitlater.infrastructure.SlackScheduledMessageDeletor
import guru.bitman.slackitlater.infrastructure.SlackSchedulesLister

class CommandFactory {
    fun createCommand(args: Argument): Command {
        val token = System.getenv("SLACK_TOKEN")
        return when (args.type) {
            ArgType.SCHEDULE -> ScheduleCommand(args, token)
            ArgType.LIST -> ListScheduledMessages(token)
            ArgType.UNKNOWN -> UnknownCommand()
            ArgType.DELETE -> DeleteCommand(args, token)
        }
    }
}

interface Command {
    fun execute()
}

class DeleteCommand(private val args: Argument, private val token: String?) : Command {
    override fun execute() {
        val deleteScheduleMessageArguments = args as DeleteScheduleMessageArgument
        val channel = deleteScheduleMessageArguments.channel
        val msgId = deleteScheduleMessageArguments.id
        println("Deleting scheduled message ${msgId} at channel ${channel}")
        DeleteScheduleMessage(SlackScheduledMessageDeletor())(channel, msgId, token)
        println("INFO: Message scheduled deleted")
    }
}

class ScheduleCommand(private val args: Argument, private val token: String?) : Command {
    override fun execute() {
        val scheduleMessageArguments = args as ScheduleMessageArguments
        val date = PrettyTimePostParser().parse(scheduleMessageArguments.`when`)
        println("Message schedule time : ${date}")
        val driver = SlackMessageScheduler(token)
        ScheduleMessage(driver)(scheduleMessageArguments.channel, scheduleMessageArguments.message, date)
        println("INFO : Message sent")
    }

}

class UnknownCommand : Command {
    override fun execute() {
        throw IllegalArgumentException("Unknown command")
    }
}

class ListScheduledMessages(private val token: String?) : Command {
    override fun execute() {
        val ids = (guru.bitman.slackitlater.actions.ListScheduledMessages(SlackSchedulesLister(token)))()
        println("----------------------------------")

        ids.forEach {
            println("schedule id : ${it.id}")
            println("channel name : ${it.channelName}")
            println("text : ${it.textSpy}")
            println("post date : ${it.postAt}")
            println("----------------------------------")
        }
    }

}
