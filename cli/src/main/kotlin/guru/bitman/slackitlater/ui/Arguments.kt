package guru.bitman.slackitlater.ui

interface ParserChain {
    fun parse(args: Array<String>): Argument
}

abstract class ParserChainLifeCycle(val next: ParserChain) : ParserChain {
    override fun parse(args: Array<String>): Argument {
        return if (canHandle(args)) {
            manageArgumentsFrom(args)
        } else {
            passControlToNextParser(args)
        }
    }

    private fun passControlToNextParser(args: Array<String>): Argument = next.parse(args)

    abstract fun manageArgumentsFrom(args: Array<String>): Argument

    abstract fun canHandle(args: Array<String>): Boolean
}

open class Argument(val type: ArgType)

enum class ArgType {
    SCHEDULE, UNKNOWN, LIST, DELETE
}
