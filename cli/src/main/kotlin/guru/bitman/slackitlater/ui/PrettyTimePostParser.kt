package guru.bitman.slackitlater.ui

import org.ocpsoft.prettytime.nlp.PrettyTimeParser
import java.util.*

class PrettyTimePostParser {
    fun parse(date: String): Date {
        val parsed = PrettyTimeParser().parse(date)
        return if ( parsed.isEmpty() ) {
            throw IllegalArgumentException()
        } else {
            parsed[0]
        }
    }
}
