package guru.bitman.slackitlater.ui

class DeleteScheduledMessageParser(private val nextChain: ParserChain) : ParserChainLifeCycle(nextChain) {

    override fun canHandle(args: Array<String>) =
            canFindInArgs(DELETE_SWITCH, args) && canFindInArgs(ID_SWITCH, args) && canFindInArgs(CHANNEL_SWITCH, args)
                    && existsIdInArgs(args) && existsChannelInArgs(args)

    override fun manageArgumentsFrom(args: Array<String>): Argument {
        val idPosition = idPositionIn(args)
        val id = args[idPosition]
        val channelPosition = channelPositionIn(args)
        val channel = args[channelPosition]
        return DeleteScheduleMessageArgument(id, channel)
    }

    private fun canFindInArgs(switch: String, args: Array<String>) = args.contains(switch)

    private fun channelPositionIn(args: Array<String>) = args.indexOf(CHANNEL_SWITCH) + 1

    private fun idPositionIn(args: Array<String>) = args.indexOf(ID_SWITCH) + 1

    private fun existsChannelInArgs(args: Array<String>): Boolean {
        val chanel = channelPositionIn(args)
        return chanel != args.size
    }

    private fun existsIdInArgs(args: Array<String>): Boolean {
        val id = idPositionIn(args)
        return id != args.size
    }

    private companion object {
        const val CHANNEL_SWITCH: String = "--channel"
        const val ID_SWITCH: String = "--id"
        const val DELETE_SWITCH: String = "--delete"
    }
}


data class DeleteScheduleMessageArgument(val id: String, val channel: String) : Argument(ArgType.DELETE)
