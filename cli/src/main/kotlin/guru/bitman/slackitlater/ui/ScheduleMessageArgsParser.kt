package guru.bitman.slackitlater.ui

class ScheduleMessageArgsParser(val nextChain: ParserChain) : ParserChainLifeCycle(nextChain) {

    override fun canHandle(args: Array<String>) = canParseArgumentFromArgs(MESSAGE, args) && canParseArgumentFromArgs(CHANNEL, args) && canParseArgumentFromArgs(WHEN, args)

    override fun manageArgumentsFrom(args: Array<String>): Argument {
        val message = parseMessageFrom(args)
        val channel = parseChannelFrom(args)
        val `when` = parseWhenFrom(args)
        return ScheduleMessageArguments(channel, message, `when`)
    }

    private fun parseWhenFrom(args: Array<String>) = parseArgumentFromArgs(WHEN, args)

    private fun parseChannelFrom(args: Array<String>) = parseArgumentFromArgs(CHANNEL, args)

    private fun parseMessageFrom(args: Array<String>) = parseArgumentFromArgs(MESSAGE, args)

    private fun parseArgumentFromArgs(arg: String, args: Array<String>): String {
        val index = args.indexOf(arg)
        return args[index + 1]
    }

    private fun canParseArgumentFromArgs(arg: String, args: Array<String>): Boolean = args.contains(arg) && isNotLastOne(arg, args)

    private fun isNotLastOne(arg: String, args: Array<String>) = args.indexOf(arg) != args.size - 1

    private companion object {
        const val MESSAGE = "--message"
        const val CHANNEL = "--channel"
        const val WHEN = "--when"
    }
}

data class ScheduleMessageArguments(val channel: String, val message: String, val `when`: String) : Argument(ArgType.SCHEDULE)
