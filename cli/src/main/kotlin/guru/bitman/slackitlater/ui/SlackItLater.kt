package guru.bitman.slackitlater.ui

import kotlin.system.exitProcess

fun main(cliArgs: Array<String>) {
    try {
        val args = parseArguments(cliArgs)
        executeCommand(args)
        communicateSuccess()
    } catch (e: Throwable) {
        showError(e.message)
        communicateFailure()
    }
}

private fun executeCommand(args: Argument) {
    createCommand(args).execute()
}

private fun parseArguments(cliArgs: Array<String>): Argument {
    return createParser().parse(cliArgs)
}

private fun showError(message: String?) {
    println("ERROR : $message")
}

private fun communicateFailure() {
    exitProcess(-1)
}

private fun communicateSuccess() {
    exitProcess(0)
}

private fun createCommand(args: Argument) = CommandFactory().createCommand(args)

private fun createParser() = ScheduleMessageArgsParser(ListChannelParser(DeleteScheduledMessageParser(InvalidArgsParser())))
