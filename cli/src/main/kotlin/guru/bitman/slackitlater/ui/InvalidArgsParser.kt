package guru.bitman.slackitlater.ui

class InvalidArgsParser : ParserChain {
    override fun parse(args: Array<String>): Argument = throw IllegalArgumentException("Invalid command")
}
