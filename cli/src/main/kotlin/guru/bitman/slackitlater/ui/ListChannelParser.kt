package guru.bitman.slackitlater.ui

class ListChannelParser(private val nextChain: ParserChain) : ParserChainLifeCycle(nextChain) {

    override fun canHandle(args: Array<String>) = args.contains("--list")

    override fun manageArgumentsFrom(args: Array<String>) = Argument(ArgType.LIST)

}
