package guru.bitman.slackitlater.ui

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DeleteScheduledMessageParserShould {

    @Test
    fun `not manage parsing when empty arguments`() {
        val args = arrayOf<String>();
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args)

        thenCannotManageParsing(argument)
    }

    @Test
    fun `not manage parsing if only delete argument is present`() {
        val args = arrayOf("--delete");
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args)

        thenCannotManageParsing(argument)
    }

    @Test
    fun `not manage parsing if no id argument present`() {
        val args = arrayOf("--delete", "--channel", "channel");
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args)

        thenCannotManageParsing(argument)
    }

    @Test
    fun `not manage parsing if no channel argument present`() {
        val args = arrayOf("--delete", "--id", "id");
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args)

        thenCannotManageParsing(argument)
    }

    @Test
    fun `accept arguments when delete and id arguments are present`() {
        val args = arrayOf("--delete", "--channel", "channel-name", "--id", "id-to-delete");
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args)

        assertEquals(ArgType.DELETE, argument.type)
    }

    @Test
    fun `return channel and message id to delete`() {
        val args = arrayOf("--delete", "--channel", "channel-name", "--id", "id-to-delete");
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args) as DeleteScheduleMessageArgument

        assertEquals("id-to-delete", argument.id)
        assertEquals("channel-name", argument.channel)
    }

    @Test
    fun `not manage parsing when id not present`() {
        val args = arrayOf("--delete", "--channel", "channel-name", "--id");
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args)

        thenCannotManageParsing(argument)
    }

    @Test
    fun `not manage parsing when channel not present`() {
        val args = arrayOf("--delete", "--id", "id", "--channel");
        val argument = DeleteScheduledMessageParser(DummyTestingParser()).parse(args)

        thenCannotManageParsing(argument)
    }

    private fun thenCannotManageParsing(argument: Argument) {
        assertEquals(ArgType.UNKNOWN, argument.type)
    }
}