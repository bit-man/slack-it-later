package guru.bitman.slackitlater.ui

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class ScheduleMessageArgsParserShould {
    private lateinit var parsedArgs: Argument

    @Test
    fun `not allow no args`() {
        val args: Array<String> = arrayOf()
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `identify message scheduling parameters`() {
        val args: Array<String> = arrayOf("--message", "Hi, it's me!", "--channel", "my-channel", "--when", "tomorrow")
        whenParsing(args)
        assertEquals(ArgType.SCHEDULE, parsedArgs.type)
    }

    @Test
    fun `manage parse when channel, message and when`() {
        val args: Array<String> = arrayOf("--message", "Hi, it's me!", "--channel", "my-channel", "--when", "tomorrow")
        whenParsing(args)
        assertEquals(ArgType.SCHEDULE, parsedArgs.type)
    }

    @Test
    fun `cannot manage parse when not channel`() {
        val args: Array<String> = arrayOf("--message", "Hi, it's me!", "--when", "tomorrow")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `pass control when cannot manage parse`() {
        val args: Array<String> = arrayOf("--message", "Hi, it's me!", "--when", "tomorrow")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `cannot manage parse when not message`() {
        val args: Array<String> = arrayOf("--channel", "my-channel", "--when", "tomorrow")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `cannot manage parse when not when`() {
        val args: Array<String> = arrayOf("--message", "Hi, it's me!", "--channel", "my-channel")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `parse channel, message and when`() {
        val args: Array<String> = arrayOf("--message", "Hi, it's me!", "--channel", "my-channel", "--when", "tomorrow")
        whenParsing(args)
        val scheduleMessageArguments = parsedArgs as ScheduleMessageArguments
        assertEquals("my-channel", scheduleMessageArguments.channel)
        assertEquals("Hi, it's me!", scheduleMessageArguments.message)
        assertEquals("tomorrow", scheduleMessageArguments.`when`)
    }

    @Test
    fun `fail when channel only`() {
        val args: Array<String> = arrayOf("--channel", "my-channel")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `should call next in chain  when message only`() {
        val args: Array<String> = arrayOf("--message", "message")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `fail when date only`() {
        val args: Array<String> = arrayOf("--when", "in 2 days")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `should call next in chain  when message parameter but no message content`() {
        val args: Array<String> = arrayOf("--chanel", "--when", "tomorrow", "my-channel", "--message")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `should call next in chain when channel parameter but no channel name`() {
        val args: Array<String> = arrayOf("--message", "message", "--when", "tomorrow", "--channel")
        whenParsing(args)
        thenCannotManageParsing()
    }

    @Test
    fun `should call next in chain when when parameter but no date`() {
        val args: Array<String> = arrayOf("--message", "message", "--channel", "my-chanel", "--when")
        whenParsing(args)
        thenCannotManageParsing()
    }

    private fun whenParsing(args: Array<String>) {
        parsedArgs = ScheduleMessageArgsParser(DummyTestingParser()).parse(args)
    }


    private fun thenCannotManageParsing() {
        assertEquals(ArgType.UNKNOWN, parsedArgs.type)
    }

}
