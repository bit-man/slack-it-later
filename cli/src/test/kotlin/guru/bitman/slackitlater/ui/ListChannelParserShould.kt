package guru.bitman.slackitlater.ui

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ListChannelParserShould {

    private lateinit var parsedArguments: Argument

    @Test
    fun `reject empty args`() {
        val args = arrayOf<String>()
        whenParsing(args)
        thenCannotParse()
    }

    @Test
    fun `parse list parameters`() {
        val args = arrayOf<String>("--list")
        whenParsing(args)
        thenParametersRecognized()
    }

    @Test
    fun `parse list even when extra parameters`() {
        val args = arrayOf<String>("--list", "or-else")
        whenParsing(args)
        thenParametersRecognized()
    }

    private fun thenCannotParse() {
        assertEquals(ArgType.UNKNOWN, parsedArguments.type)
    }

    private fun whenParsing(args: Array<String>) {
        parsedArguments = ListChannelParser(DummyTestingParser()).parse(args)
    }

    private fun thenParametersRecognized() {
        assertEquals(ArgType.LIST, parsedArguments.type)
    }
}