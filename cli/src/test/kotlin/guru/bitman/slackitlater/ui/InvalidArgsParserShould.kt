package guru.bitman.slackitlater.ui

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class InvalidArgsParserShould {

    @Test
    fun `throw exception on parse`() {
        assertThrows<IllegalArgumentException> { val args = InvalidArgsParser().parse(arrayOf()) }
    }
}