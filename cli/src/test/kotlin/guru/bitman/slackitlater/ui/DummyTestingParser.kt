package guru.bitman.slackitlater.ui

class DummyTestingParser : ParserChain {
    override fun parse(args: Array<String>): Argument {
        return UnknownArguments(ArgType.UNKNOWN)
    }
}


data class UnknownArguments(val argType: ArgType) : Argument(argType)
