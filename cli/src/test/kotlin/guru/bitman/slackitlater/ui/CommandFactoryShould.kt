package guru.bitman.slackitlater.ui

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class CommandFactoryShould {
    private lateinit var command: Command
    private lateinit var factory: CommandFactory

    @BeforeEach
    private fun setup() {
        factory = CommandFactory()
    }

    @Test
    fun `given list argument a list command is created`() {
        val args = Argument(ArgType.LIST)
        whenCommandIsCreated(args)
        assertEquals(ListScheduledMessages::class.java, command.javaClass)
    }

    @Test
    fun `given schedule argument a schedule command is created`() {
        val args = Argument(ArgType.SCHEDULE)
        whenCommandIsCreated(args)
        assertEquals(ScheduleCommand::class.java, command.javaClass)
    }

    @Test
    fun `given unkonwn argument an unknown command is created`() {
        setup()
        val args = Argument(ArgType.UNKNOWN)
        whenCommandIsCreated(args)
        assertEquals(UnknownCommand::class.java, command.javaClass)
    }

    private fun whenCommandIsCreated(args: Argument) {
        command = factory.createCommand(args)
    }
}