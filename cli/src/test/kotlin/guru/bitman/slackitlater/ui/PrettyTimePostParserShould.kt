package guru.bitman.slackitlater.ui

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class PrettyTimePostParserShould {
    @Test
    fun `parse tomorrow`() {
        val parser = PrettyTimePostParser()
        val parsedDate = parser.parse("tomorrow")
        assertNotNull(parsedDate)
    }

    @Test
    fun `fail when no parsable date`() {
        val parser = PrettyTimePostParser()
        val executable = { val parsed = parser.parse("non-parsable") }
        assertThrows<IllegalArgumentException>("Should throw Exception", executable)

    }
}